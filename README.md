# Atlassian Template Renderer Plugin

## Description

Use the Atlassian Template Renderer (ATR) to render your textual content. ATR is a library that
provides an abstraction on top of various template engines, making it easier to use the template
engines. For example, instead of having to create a VelocityEngine object and configure it, we
provide a factory to do that for you.

See the [Javadoc](https://docs.atlassian.com/atlassian-template-renderer/) and the
[documentation](https://developer.atlassian.com/display/DOCS/Atlassian+Template+Renderer).

## Atlassian Developer?

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/ATR).

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/ATR).

### Documentation

[Atlassian Template Renderer Documentation](https://developer.atlassian.com/display/DOCS/Atlassian+Template+Renderer)
